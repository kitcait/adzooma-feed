import { URLs } from '../urls'

export const feedsModule = {
  namespaced: true,

  state: {
    feeds: [],
    currentFeed: {},
  },

  mutations: {
    setFeeds (state, feeds) {
      state.feeds = feeds
    },
    setCurrentFeed (state, feed) {
      state.currentFeed = feed
    },
    addFeed (state, feed) {
      state.feeds.push(feed.data)
    }
  },

  actions: {
    async addFeed (context, newFeed) {
      await axios.post(URLs.feeds.root, newFeed).then(
        response => {
          context.commit('addFeed', response.data)
        }
      )
    },
    async getFeeds (context) {
      await axios.get(URLs.feeds.root).then(
        response => {
          context.commit('setFeeds', response.data)
        }
      )
    },
    async getCurrentFeed (context, feedId) {
      await axios.get(URLs.feeds.byId(feedId), { params: { id: feedId} }).then(
        response => {
          context.commit('setCurrentFeed', response.data)
        }
      )
    }
  }
}
