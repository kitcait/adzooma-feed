/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/* === Import Statements === */
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import { routes } from './routes'
import { feedsModule } from './stores/feedsModule'

/* === General Vue Requirements === */
require('./bootstrap');
window.Vue = require('vue');

/* === Use Statements === */
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(Vuetify)

/* === Vue configuration === */
const router = new VueRouter({
  mode: 'history',
  routes: routes
})

const store = new Vuex.Store({
  modules: {
    feeds: feedsModule,
  }
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/* === Vuetify Theme Setup === */

const vuetify = new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#48A54C',
        secondary: '#ED4D49',
        accent: '#FDB72D',
        error: '#f44336',
        warning: '#ffc107',
        info: '#00d4ff',
        success: '#4caf50',
        mediumGrey: '#eceff1',
        darkGrey: '#444',
        white: '#fff'
      },
      dark: {
        primary: '#48A54C',
        secondary: '#ED4D49',
        accent: '#eeeeee',
        error: '#f44336',
        warning: '#ffc107',
        info: '#00d4ff',
        success: '#4caf50',
        mediumGrey: '#444',
        blue: '#7a7afe'
      }
    }
  }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  router,
  vuetify,
  store,
  el: '#app'
})

