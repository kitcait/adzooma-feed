import DashboardView from './views/DashboardView'
import FeedView from './views/FeedView'

export const routes = [
  {
    path: '/dashboard',
    name: 'DashboardView',
    component: DashboardView
  },
  {
    path: '/single-feed/:id',
    name: 'FeedView',
    component: FeedView
  }
]
