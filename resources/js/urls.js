export const URLs = {
    feeds: {
      root: '/api/feeds/',
      byId: function (id) {
        return '/api/feeds/' + id
      }
    },
}
