<?php

namespace App\Http\Requests\Feed;

use App\Http\Requests\AbstractRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Feed;

class ShowRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Feed::where('id', $this->id)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:feeds,id'
        ];
    }
}
