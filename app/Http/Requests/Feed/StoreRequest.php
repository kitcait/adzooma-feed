<?php

namespace App\Http\Requests\Feed;

use App\Http\Requests\AbstractRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required|string|url'
        ];
    }
}
