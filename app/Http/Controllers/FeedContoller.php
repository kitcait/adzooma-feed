<?php

namespace App\Http\Controllers;

use App\Models\Feed;
use Illuminate\Http\Request;
use App\Http\Requests\Feed\ShowRequest as FeedShowRequest;
use App\Http\Requests\Feed\StoreRequest as FeedStoreRequest;
use App\Http\Resources\FeedResource;
use Exception;
use Illuminate\Support\Facades\Auth;

class FeedContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds = Feed::get(); // Autoscoped to my own feeds
        return response(
            FeedResource::collection($feeds)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedStoreRequest $request)
    {
        $fields = $request->getRequestFields();
        $fields['user_id'] = Auth::id();
        return FeedResource::make(Feed::create($fields));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FeedShowRequest $request)
    {
        try {
            $feed = Feed::findOrFail($request->id);
            return response(FeedResource::make($feed));
        } catch (Exception $e) {
            return response('We were unable to find any feeds belonging to you by that ID', 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
