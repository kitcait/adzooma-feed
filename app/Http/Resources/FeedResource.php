<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FeedResource extends JsonResource
{
    /**
             * Transform the resource into an array.
             *
             * @param  \Illuminate\Http\Request  $request
             * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            /** With more time I'd put this into a service class and look at optimisation, throw some caching in too
             * probably. Load time is about 5s for 10 feeds on wifi so needs to be sped up else mobile users will suffer
             */
            'rss' => simplexml_load_file($this->url)->channel
        ];
    }
}
