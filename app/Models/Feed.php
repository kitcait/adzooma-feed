<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\OwnFeeds;

class Feed extends Model
{
    use HasFactory;

    /**
     * Apply the OwnFeeds scope to all queries through this model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new OwnFeeds());
    }

    protected $fillable = [
        'user_id',
        'url',
    ];
}
