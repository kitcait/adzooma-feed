## Setup

- Clone the repo from bitbucket with "git clone git@bitbucket.org:kitcait/adzooma-feed.git"
- Set the project up in you virtual provider of choice (Homestead/Docker/Valet etc)
- run "composer install"
- run "cp .env.example .env"
- edit the .env to add your database details
- run "php artisan migrate"
- run "npm install && npm run dev" to install the dependencies and compile the assets
- navigate to whatever URL or IP address you've configured your VM to run at.
- Create a user and enjoy the wonderfully useful and definitely fully featured RSS feed application
- (I recommend XKCD personally, https://xkcd.com/rss.xml)

